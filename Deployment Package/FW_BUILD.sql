-- THIS SCRIPT WILL RUN IN SEQUENCE EXECUTING ALL ENTRIES BELOW IN THE ORDER DISPLAYED
-- GETTING STARTED SUMMARY
--  1) SET TURN ON SQLCMD MODE 
--  2) UPDATE VALUES OF VARIABLES SECTION TO RELFECT THE CURRENT ENVIORNMENT
--  3) SET FLAGS IF NEEDED FOR ENVIRONMENT
--
--***********************************************************
--
-- STEP 1 ---- GO TO THE QUERY MENU > SQLCMD
--
--***********************************************************
--
-- STEP 2 ---- UPDATE VALUES TO MATCH LOCAL SYSTEMS
--
--
-- PATH TO DIRECTORY WHERE THIS BUILD SCRIPT IS LOCATED
:setvar SCRIPTS_PATH "C:\Repositories\fisherframework\Deployment Package\"
-- RELATIVE PATH TO SUBDIRECTORIES **NOTE DON'T CHANGE UNLESS REQUIRED**
:setvar TABLE_SCRIPTS_DIR "Tables\"
:setvar DATA_SCRIPTS_DIR "Data\"
:setvar VIEW_SCRIPTS_DIR "Views\"
:setvar FUNCT_SCRIPTS_DIR "Functions\"
:setvar PROC_SCRIPTS_DIR "Procedures\"
:setvar SEC_SCRIPTS_DIR "Security\"
--
-- NAME OF FRAMWORK DATABASE BEING CREATED
:setvar FWDATABASE "FT_Framework"
-- $(FWDATABASE)
--
-- DATABASE AND LOG FILE LOCATION (.mdb and .log)
:setvar FWDATABASEFILES "C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\"
-- $(FWDATABASEFILES)
--
-- NAME OF TARGET ERP DATABASE
:setvar TARGETDB "SBODEMOUS"
-- $(TARGETDB)
--
-- NAMES OF USER DEFINED FIELDS TO MAP DATA IN TARGET DB	
-- FIELD TO STORE WEBSTORE CUSTOMER ID 
:setvar PartnerWebIDColumn "U_FT_WebCustomerID"
-- $(PartnerWebIDColumn)
--
-- FIELD TO STORE WEBSTORE ITEM ID OF ITEM **THIS IS NOT THE SKU**
:setvar ItemWebIDColumn "U_FT_WebItemID"
-- $(ItemWebIDColumn)
--
-- FIELD TO STORE THE WEBSTORE ID FOR THE ORDER **THIS IS NOT THE USER FRIENDLY ORDER NUMBER**
:setvar MarketingDocWebIDColumn "U_FT_WebOrderID"
-- $(MarketingDocWebIDColumn)		
-- 
-- RUN TARGET DB Precheck SET VALUE TO 'Y'
:setvar RunTargetDBCheck "Y"
--
-- NAME OF COMMON ERP DATABASE
:setvar COMMONDB "SBO-COMMON"
-- $(COMMONDB)
--
--***********************************************************
--
-- STEP 3 ---- SET FLAGS DEPENDING ON THE DESIRED INSTALL
--
-- 
-- SET '1' TO DELETE THE EXISTING INCLUDING ALL DATA AND RECREATE TABLES, VIEWS AND PROCEDURES
-- SET '0' TO UPDATE THE EXISTING DATABASE TABLES PRESERVING DATA, RECREATE VIEWS AND RECREATE PROCEDURES
:setvar NEWINSTALL "1"
--
--
--
--***********************************************************
--
--
--
/***************************************************************
*********      DO NOT EDIT BELOW THIS LINE       ***************
*********     THERE IS NO USER EDITABLE CODE     ***************
****************************************************************/
:ON Error EXIT

DECLARE @RunDBCheck nvarchar(1)
SET @RunDBCheck = '$(RunTargetDBCheck)'

if @RunDBCheck = 'Y'
	GOTO precheck
else
	GOTO precheck_passed

-- || TARGET DATABASE CHECK FOR REQUIRED USER DEFINED FIELDS
--
--
precheck:
DECLARE @ERRCount int
SET @ERRCount = 0

USE $(TARGETDB);

PRINT 'Checking Tables for User Defined Fields' 
--CHECK THAT BP WEB ID FIELD EXISTS
IF EXISTS(SELECT * FROM  INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OCRD' AND COLUMN_NAME IN ( '$(PartnerWebIDColumn)' ))
	PRINT '$(TARGETDB).OCRD.$(PartnerWebIDColumn)  -- EXISTS' 
ELSE
	BEGIN	 --FIELD IS REQUIRED FOR VIEWS
		PRINT '$(TARGETDB).OCRD.$(PartnerWebIDColumn)  -- NOT FOUND'
		SET @ERRCount = @ERRCount + 1
	END

--CHECK THAT ITEM WEB ID FIELD EXISTS
IF EXISTS(SELECT * FROM  INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'OITM' AND COLUMN_NAME IN ( '$(ItemWebIDColumn)' ))
	PRINT '$(TARGETDB).OITM.$(ItemWebIDColumn)  -- EXISTS' 
ELSE
	BEGIN	 --FIELD IS REQUIRED FOR VIEWS
		PRINT '$(TARGETDB).OITM.$(ItemWebIDColumn)  -- NOT FOUND'
		SET @ERRCount = @ERRCount + 1
	END

--CHECK THAT MARKETING DOCUMENT WEB ID EXITS
IF EXISTS(SELECT * FROM  INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'ORDR' AND COLUMN_NAME IN ( '$(MarketingDocWebIDColumn)' ))
	PRINT '$(TARGETDB).ORDR.$(MarketingDocWebIDColumn)  -- EXISTS' 
ELSE
	BEGIN	 --FIELD IS REQUIRED FOR VIEWS
		PRINT '$(TARGETDB).ORDR.$(MarketingDocWebIDColumn)  -- NOT FOUND'
		SET @ERRCount = @ERRCount + 1
	END

--PRINT @ERRCount
IF @ErrCount = 0
	GOTO precheck_passed

precheck_failed:
--RAISERROR ('Something Bad Happened', 11, 1)
RAISERROR ('Required fields are missing from the database cannot continue', 11,1)
RETURN;


precheck_passed:	-- when precheck runs if all required fields are found then database is created


--|| BUILD DATABASE STRUCTURE 
--|| STEPS:
--||  1) ROLLBACK AND DROP EXISTING DB THEN CREATE NEW DB
--||  2) ADD TABLES
--||  3) ADD DEFAULT DATA
--||  4) ADD VIEWS
--||	a) STANDARD VIEWS
--||    b) TAX VIEWS (OPTIONAL)
--||  5) ADD FUNCTIONS
--||  6) ADD STORED PROCEDURES
IF EXISTS (SELECT name FROM master.dbo.sysdatabases WHERE name = '$(FWDATABASE)')
BEGIN
	PRINT ''
	PRINT '$(FWDATABASE) EXISTS'
	PRINT 'DISCONNECTING USERS AND ROLLBACK OF ACTIVE TRANSACTIONS'
	USE [MASTER]
	ALTER DATABASE $(FWDATABASE) SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	PRINT '$(FWDATABASE) IN SINGLE USER MODE' 
END

--|| REBUILD DATABASE
:r $(SCRIPTS_PATH)\Create_Database.sql
USE $(FWDATABASE)

--|| ADD TABLES
PRINT ''
PRINT '** ADD TABLES **'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\ERPDefaults.sql
PRINT 'ADDED - $(FWDATABASE).ERPDefaults'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\FreightMapping.sql
PRINT 'ADDED - $(FWDATABASE).FreightMapping'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\Items.sql
PRINT 'ADDED - $(FWDATABASE).Items'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\OrderFulfillments.sql
PRINT 'ADDED - $(FWDATABASE).OrderFulfillments'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\OrderLines.sql
PRINT 'ADDED - $(FWDATABASE).OrderLines'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\Orders.sql
PRINT 'ADDED - $(FWDATABASE).Orders'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\OrderTransactions.sql
PRINT 'ADDED - $(FWDATABASE).OrderTransactions'
:r $(SCRIPTS_PATH)$(TABLE_SCRIPTS_DIR)\TestUsers.sql
PRINT 'ADDED - $(FWDATABASE).TestUsers'

--|| ADD DATA
PRINT ''
PRINT '** LOAD DEFAULT DATA **'
:r $(SCRIPTS_PATH)$(DATA_SCRIPTS_DIR)\Load_Values.sql

--|| ADD VIEWS
PRINT ''
Print '** ADD VIEWS **'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_ItemLookup.sql
PRINT 'ADDED - V_ItemLookup'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_CustomerLookup.sql
PRINT 'ADDED - V_CustomerLookup'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_NextCustomerCode.sql
PRINT 'ADDED - V_NextCustomerCode'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_FreightLookup.sql
PRINT 'ADDED - V_FreightLookup'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_ShippingLookup.sql
PRINT 'ADDED - V_ShippingLookup'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_OrderAddresses.sql
PRINT 'ADDED - V_OrderAddresses'
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_UnfulfilledOrders.sql
PRINT 'ADDED - V_UnfulfilledOrders'

--PROCESS TAX RELATED ENTRIES
:r $(SCRIPTS_PATH)$(VIEW_SCRIPTS_DIR)\V_TaxCodeLookup.sql
PRINT 'ADDED - V_TaxCodeLookup'

--|| ADD FUNCTIONS 
PRINT ''
PRINT '** ADD FUNCTIONS **'
:r $(SCRIPTS_PATH)$(FUNCT_SCRIPTS_DIR)\GetAddress.sql
PRINT 'ADDED - FUNCT GetAddress'
:r $(SCRIPTS_PATH)$(FUNCT_SCRIPTS_DIR)\GetHTMLChar.sql
PRINT 'ADDED - GetHTMLChar'
:r $(SCRIPTS_PATH)$(FUNCT_SCRIPTS_DIR)\HTMLSafe.sql
PRINT 'ADDED - HTMLSafe'

--|| ADD PROCEDURES
PRINT ''
PRINT '** ADD PROCEDURES **'
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_GetReadyOrders.sql
PRINT 'ADDED - FT_SP_GetReadyOrders'		
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_InsertOrder.sql
PRINT 'ADDED - FT_SP_InsertOrder'					 
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_InsertItem.sql
PRINT 'ADDED - FT_SP_InsertOrder'					 
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_InsertTransactions.sql
PRINT 'ADDED - FT_SP_InsertTransactions'			  
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_UpdateCustomer.sql
PRINT 'ADDED - FT_SP_UpdateCustomer'				  
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_UpdateCustomerAddress.sql
PRINT 'ADDED - FT_SP_UpdateCustomerAddress'		   
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_UpdateFreight.sql
PRINT 'ADDED - FT_SP_UpdateFreight'				   
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_UpdateFulfillmentStatus.sql
PRINT 'ADDED - FT_SP_UpdateFulfillmentStatus'	   
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_UpdateSyncStatus.sql
PRINT 'ADDED - FT_SP_UpdateSyncStatus'			   
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_Validate_Items.sql
PRINT 'ADDED - FT_SP_Validate_Items'			   
:r $(SCRIPTS_PATH)$(PROC_SCRIPTS_DIR)\FT_SP_ValidateTax.sql
PRINT 'ADDED - FT_SP_ValidateTax'

--|| MANAGE USER PERMISSIONS
PRINT ''
PRINT '** ADD/UPDATE USER **'
:r $(SCRIPTS_PATH)$(SEC_SCRIPTS_DIR)\FT_TCU.sql
PRINT ''
Print 'Execution Finished '

   
