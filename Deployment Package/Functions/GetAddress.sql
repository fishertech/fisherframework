USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE FUNCTION [dbo].[GetAddress]
(	
	-- Add the parameters for the function here
	@CardCode nvarchar(15),
	@AddressType nvarchar(1),
	@AddressID nvarchar(50)
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	select * from $(TARGETDB).dbo.CRD1
    Where
    [CardCode] = @CardCode
    AND [AdresType] = @AddressType
    AND [Address] = @AddressID
)
GO


