USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE FUNCTION [dbo].[HTMLSafe](@Request nvarchar(max), @Level int)
RETURNS nvarchar(max)
as
BEGIN
--Returns Encoded String with all special characters encoded with HTML safe encodings
DECLARE @RequestLength int;
DECLARE @Returnvalue nvarchar(max);
DECLARE @position int;
DECLARE @NextChar nvarchar(1);

SET @Returnvalue = ''
SET @RequestLength = len(@Request)
SET @position = 1


	If @Request COLLATE Latin1_General_BIN  like N'%[^a-zA-Z0-9 ]%' COLLATE Latin1_General_BIN
		IF @RequestLength > 10 and @Level < 4
			SET @Returnvalue = dbo.HTMLSafe(substring(@Request,0,@RequestLength/2), @Level + 1) + dbo.HTMLSafe(substring(@Request,@RequestLength/2,@RequestLength),@Level + 1)
		ELSE
			WHILE @position <= DATALENGTH(@Request)  
			BEGIN  
				SET @Returnvalue = Concat(@Returnvalue,dbo.GetHTMLChar(SUBSTRING(@Request,@position,1)))
				SELECT @position = @position + 1  
			END;
	ELSE
	SET @Returnvalue =  @Request

RETURN @Returnvalue
END
GO