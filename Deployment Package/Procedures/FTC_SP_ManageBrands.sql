USE $(FWDATABASE)
GO

/* -- USE WHEN REVISING EXISTING STORED PROCEDURE 
DROP PROCEDURE [dbo].[FTC_SP_ManageBrands]
GO
*/




CREATE PROCEDURE [dbo].[FTC_SP_ManageBrands]

@Name nvarchar(100),
@Action nvarchar(1), -- Action values C = Create , U = Update, D = Delete
@Store_Id nvarchar(50) = NULL,
@Store_URI nvarchar(max) = NULL,
@Description nvarchar(30) = NULL,
@ERP_Id nvarchar(50), -- only avalable on Create
@Last_Store_Update datetimeoffset(7) = NULL,
@Last_ERP_Update datetimeoffset(7) = NULL,
@Sync_Status nvarchar(50) = 'Pending',
@Country_Code nvarchar(2) = NULL

AS

DECLARE
@Created_At datetimeoffset(7) = GETDATE(),
@Updated_At datetimeoffset(7) = GETDATE()

IF @Action = 'U' -- UPDATE
	BEGIN
		UPDATE [dbo].[Brands]
		   SET [Store_Id] = CASE WHEN @Store_Id = NULL THEN (Select Store_Id from Brands where Name = @Name) ELSE @Store_Id END
			  ,[Store_URI] = CASE WHEN @Store_URI = NULL THEN (Select Store_URI from Brands where Name = @Name) ELSE @Store_URI END
			  ,[Description] = CASE WHEN @Description = NULL THEN (Select [Description] from Brands where Name = @Name) ELSE @Description END
              ,[Country] = CASE WHEN @Country_Code = NULL THEN (Select [Country] from Brands where Name = @Name) ELSE @Country_Code END
			  ,[Last_Store_Update] = CASE WHEN @Last_Store_Update = NULL THEN (Select Last_Store_Update from Brands where Name = @Name) ELSE @Last_Store_Update END
			  ,[Last_ERP_Update] = CASE WHEN @Last_ERP_Update = NULL THEN (Select Last_ERP_Update from Brands where Name = @Name) ELSE @Last_ERP_Update END
			  ,[Sync_Status] = CASE WHEN @Sync_Status = NULL THEN (Select Store_Id from Brands where Name = @Name) ELSE @Sync_Status END
			  ,[Updated_At] = @Updated_At
			WHERE Name = @Name
		RETURN
	END

IF @Action = 'C' --CREATE
	BEGIN
		INSERT INTO [dbo].[Brands]
			([Name]
			,[Store_Id]
			,[Store_URI]
            ,[Country]
			,[Description]
            ,[ERP_Id]
			,[Last_Store_Update]
			,[Last_ERP_Update]
			,[Sync_Status]
			,[Created_At]
			,[Updated_At])
		VALUES
			(@Name,
			@Store_Id,
			@Store_URI,
            @Country_Code,
			@Description,
            @ERP_Id,
			@Last_Store_Update,
			@Last_ERP_Update,
			@Sync_Status,
			@Created_At,
			@Updated_At)
		RETURN
	END			

IF @Action = 'D' -- DELETE
BEGIN
	DELETE FROM [dbo].[Brands]
		  WHERE Name = @Name
	RETURN
END
GO