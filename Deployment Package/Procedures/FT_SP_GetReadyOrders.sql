USE $(FWDATABASE)
GO
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql

CREATE PROCEDURE [dbo].[FT_SP_GetReadyOrders]
--PARAMS
    @Tax_Type bit,
	@ID int

AS
 --DECALRED VARS
DECLARE
    @LineTotal FLOAT

SELECT [Orders].[Id]
      ,[Orders].[Store_Id]
      ,[Orders].[Store_URI]
	  ,[OrderLines].[Line_Id]
      ,[OrderLines].[Item_Price]
      ,[OrderLines].[Item_Sku]
      ,[OrderLines].[Item_Quantity]
      ,[Orders].[ERP_Id]
      ,[Orders].[Store_Customer]
      ,[Orders].[ERP_Customer]
      ,[Orders].[Store_Status]
      ,[Orders].[Sync_Status]
      ,[Orders].[Created_At]
      ,[Orders].[Updated_At]
      ,[Orders].[Note]
      ,[Orders].[Accepts_Marketing]
      ,[Orders].[Bill_Address_ID]
      ,[Orders].[Bill_First_Name]
      ,[Orders].[Bill_Last_Name]
      ,[Orders].[Bill_Company]
      ,[Orders].[Bill_Address]
      ,[Orders].[Bill_Address2]
      ,[Orders].[Bill_Address3]
      ,[Orders].[Bill_City]
      ,[Orders].[Bill_Province_Code]
      ,[Orders].[Bill_Province_Name]
      ,[Orders].[Bill_Post_Code]
      ,[Orders].[Bill_Country_Code]
      ,[Orders].[Bill_Country_Name]
      ,[Orders].[Bill_Latitude]
      ,[Orders].[Bill_Longitude]
      ,[Orders].[Cart_Token]
      ,[Orders].[Email]
      ,[Orders].[Tags]
      ,[Orders].[Pay_Amount]
      ,[Orders].[Pay_Authoirzation]
      ,[Orders].[Pay_Gateway]
      ,[Orders].[Pay_Kind]
      ,[Orders].[Pay_Id]
      ,[Orders].[Pay_Status]
      ,[Orders].[Financial_Status]
      ,[Orders].[Taxes_Included]
      ,[Orders].[Ship_Address_ID]
      ,[Orders].[Ship_First_Name]
      ,[Orders].[Ship_Last_Name]
      ,[Orders].[Ship_Company]
      ,[Orders].[Ship_Address]
      ,[Orders].[Ship_Address2]
      ,[Orders].[Ship_Address3]
      ,[Orders].[Ship_City]
      ,[Orders].[Ship_Province_Code]
      ,[Orders].[Ship_Province_Name]
      ,[Orders].[Ship_Post_Code]
      ,[Orders].[Ship_Country_Code]
      ,[Orders].[Ship_Country_Name]
      ,[Orders].[Ship_Latitude]
      ,[Orders].[Ship_Longitude]
      ,[Orders].[Fulfillment_Status]
      ,[Orders].[Total_Discount]
      ,[Orders].[Total_Lines] + [Orders].[Total_Tax] AS [Total_Lines]
	  ,[Orders].[Total_Freight]
      ,[Orders].[Total_Order]
	  ,(SELECT CASE WHEN @Tax_Type = 1 THEN 0.00 ELSE [Orders].[Total_Tax_Rate] END as [Total_Tax_Rate]) as [Total_Tax_Rate]
	  ,(SELECT CASE WHEN @Tax_Type = 1 THEN 0.00 ELSE [Orders].[Total_Tax_Validated] END as [Total_Tax_Validated]) as [Total_Tax_Validated]
	  ,(SELECT CASE WHEN @Tax_Type = 1 THEN 'Avatax' ELSE [Orders].[Tax_Code] END as [Tax_Code]) as [Tax_Code]
	  ,[ERPDefaults].[Order_CCEV]
	  ,[ERPDefaults].[Order_Confirmed]
	  ,[ERPDefaults].[Order_ExpenseCode]
	  ,[ERPDefaults].[Order_Installments]
	  ,[ERPDefaults].[Order_OwnerCode]
	  ,[ERPDefaults].[Order_PayType]
	  ,[ERPDefaults].[Order_TaxStatus]
	  ,[ERPDefaults].[Order_TaxType]  
  FROM (SELECT Id, Line_Id,Item_Price,Item_Sku,Item_Quantity
        FROM OrderLines WHERE Id = @ID
        UNION (Select @ID,(SELECT max(Line_Id) + 1 FROM OrderLines WHERE Id = @ID),(SELECT Total_Tax_Validated FROM Orders WHERE Id = @ID),'Tax',1 WHERE (SELECT Total_Tax FROM Orders WHERE Id = @Id) <> 0)
        UNION (SELECT @ID,(SELECT max(Line_Id) + 2 FROM OrderLines WHERE Id = @Id),(SELECT Total_Tax - Total_Tax_Validated FROM Orders WHERE Id = @Id),'Tax-Difference',1 WHERE (SELECT Total_Tax_Validated - Total_Tax FROM Orders WHERE Id = @Id) <> 0)) As OrderLines
  LEFT JOIN [dbo].[Orders] On Orders.Id = OrderLines.Id
  LEFT JOIN [dbo].[ERPDefaults] on 1 = 1
  WHERE OrderLines.Id = @Id

--Select * from #OrderLnTemp ORDER BY Line_Id

RETURN 0
GO
