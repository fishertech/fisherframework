USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_UpdateCustomer]

@OrderId nvarchar(20)

AS

DECLARE @Updated_At datetimeoffset
DECLARE @ValidTaxAmount money
DECLARE @Id int
DECLARE @ERP_Customer nvarchar(50)
DECLARE @Cust_Valid bit
DECLARE @Store_Customer nvarchar(50)

SET @Updated_At = GETDATE()
SET @Id = CONVERT(int, @OrderId)
SET @Store_Customer = (Select Store_Customer from Orders where Id = @OrderId  collate SQL_Latin1_General_CP1_CI_AS )
SET @ERP_Customer = (Select ERP_Id from V_CustomerLookup where Store_Id = @Store_Customer  collate SQL_Latin1_General_CP1_CI_AS ) 
SET @Cust_Valid = (select case when len((Select ERP_Id from V_CustomerLookup where Store_Id = @Store_Customer)) > 0 then 1 else 0 end )


/*** Lookup customer in View based on order inforamtion if customer exists in system then ERP_Id is set and validated set true otherwise it remains false ***/

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[ERP_Customer] = @ERP_Customer
      ,[Customer_Validated] = @Cust_Valid
 WHERE [Id] = @Id and [ERP_Customer] is null


Return 0
GO
