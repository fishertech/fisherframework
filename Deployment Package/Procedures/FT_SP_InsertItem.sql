USE $(FWDATABASE)
GO
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql

-- THIS PROCEDURE WILL INSERT ITEMS INTO THE ITEMS TABLE WHEN NEW 
-- AND PERFORM UPDATES WHEN THEY ALREADY EXIST

CREATE PROCEDURE [dbo].[FT_SP_InsertItem]
      @Sku nvarchar(100),
	  @Store_Id [nvarchar](30),
      @Store_URI nvarchar(max),
      @ERP_Id nvarchar(30),
      @Store_Status nvarchar(50),
      @Sync_Status nvarchar(50),
	  @SyncSource nvarchar(20) -- sync source from SHOP or from ERP the value supplied should only be either[ SHOP ] or [ ERP ]
AS

DECLARE @Updated_At datetimeoffset(7)
DECLARE @Created_At datetimeoffset(7)
DECLARE @Last_Store_Sync datetimeoffset(7)
DECLARE @last_Erp_Sync datetimeoffset(7)


SET @Updated_At = CONVERT(datetimeoffset,GETDATE(),120)
SET @Created_At = CONVERT(datetimeoffset,GETDATE(),120)
SET @Last_Store_Sync = (select Last_Store_Sync from dbo.Items where Sku = @Sku)
SET @Last_ERP_Sync = (select Last_ERP_Sync from dbo.Items where Sku = @Sku)

	IF NOT EXISTS (Select Sku from dbo.Items where Sku = @Sku) -- Create if no item or Update if item found
		BEGIN
			INSERT INTO [dbo].[Items]
					   ([Sku]
					   ,[Store_Id]
					   ,[Store_URI]
					   ,[ERP_Id]
					   ,[Store_Status]
					   ,[Sync_Status]
					   ,[Created_At]
					   ,[Updated_At]
					   ,[Last_Store_Sync]
					   ,[Last_ERP_Sync])
				 VALUES
					   (@Sku
					   ,@Store_Id
					   ,@Store_URI
					   ,@ERP_ID
					   ,@Store_Status
					   ,@Sync_Status
					   ,@Created_At
					   ,@Updated_At
					   ,(SELECT CASE WHEN @SyncSource = 'SHOP' THEN @Updated_At ELSE NULL END)
					   ,(SELECT CASE WHEN @SyncSource = 'ERP' THEN @Updated_At ELSE NULL END)
					   )
		END
	ELSE -- PERFORM AN UPDATE
		IF @SyncSource = 'ERP'
			BEGIN  -- SET VALUES FOR ERP SOURCE
				UPDATE [dbo].[Items]
				   SET [Store_Status] = @Store_Status
					  ,[Sync_Status] = @Sync_Status
					  ,[Updated_At] = @Updated_At
					  ,[Last_ERP_Sync] = @Updated_At
				 WHERE Sku = @Sku
			END
		ELSE
			BEGIN  --SET VALUES FOR SHOP SOURCE
				UPDATE [dbo].[Items]
				   SET [Store_Status] = @Store_Status
					  ,[Sync_Status] = @Sync_Status
					  ,[Updated_At] = @Updated_At
					  ,[Last_Store_Sync] = @Updated_At
				 WHERE Sku = @Sku
			END
	RETURN
GO




