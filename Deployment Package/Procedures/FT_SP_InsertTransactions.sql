USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_InsertTransactions]

@Id int,
@Transaction_Id bigint,
@Action_Type nvarchar (50),
@Amount money ,
@Authorization nvarchar (50),
@Created_At nvarchar(50) ,
@Gateway nvarchar (30),
@Kind nvarchar (30),
@Reference_Code nvarchar (100),
@Location_id int ,
@Message nvarchar (max),
@Store_Id bigint ,
@Status nvarchar (30),
@Test nvarchar (10),
@User int ,
@Device_id nvarchar (50),
@CardNumberRef nvarchar(4),
@CardCompany nvarchar(20),
@CC_Last4 int,
@CC_ExpireMonth int,
@CC_ExpireYear int

AS

DECLARE @Updated_At datetimeoffset
DECLARE @Created_AtTime datetimeoffset

SET @Updated_At = convert(datetimeoffset,GETDATE(),120)
SET @Created_AtTime = convert(datetimeoffset,@Created_At,120)


INSERT INTO [dbo].[OrderTransactions]
           ([Id]
           ,[Transaction_Id]
           ,[Action_Type]
           ,[Amount]
           ,[Authorization]
           ,[Created_At]
           ,[Gateway]
           ,[Kind]
           ,[Updated_At]
           ,[Reference_Code]
           ,[Location_id]
           ,[Message]
           ,[Store_Id]
           ,[Status]
           ,[Test]
           ,[User]
           ,[Device_id]
		   ,[PayCardRef]
		   ,[PayCardCo]
		   ,[CC_Last4]
		   ,[CC_ExpireMonth]
		   ,[CC_ExpireYear])
     VALUES
           (@Id,
		   @Transaction_Id,
		   @Action_Type,
		   @Amount,
		   @Authorization,
		   @Created_AtTime,
		   @Gateway,
		   @Kind,
		   @Updated_At,
		   @Reference_Code,
		   @Location_id,
		   @Message,
		   @Store_Id,
		   @Status,
		   @Test,
		   @User,
		   @Device_id,
		   @CardNumberRef,
		   @CardCompany,
		   @CC_Last4,
		   @CC_ExpireMonth,
		   @CC_ExpireYear)

IF @Kind = 'sale' or @Kind = 'capture'
UPDATE [dbo].[Orders]
   SET [Transaction_Validated] = 1
      ,[Updated_At] = @Updated_At
      ,[Pay_Amount] = @Amount
      ,[Pay_Authoirzation] = @Authorization
      ,[Pay_Gateway] = @Gateway
      ,[Pay_Kind] = @Kind
      ,[Pay_Status] = @Action_Type
      ,[Financial_Status] = @Kind
 WHERE [Id] = @id

Return 0
GO