USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_UpdateCustomerAddress]

@OrderId nvarchar(20),
@AddressType nvarchar(1),
@AddressId nvarchar(30)

AS

DECLARE @Updated_At datetimeoffset
DECLARE @Id int
DECLARE @AddrType nvarchar(1)

SET @Updated_At = GETDATE()
SET @Id = CONVERT(int, @OrderId)
SET @AddrType = UPPER(@AddressType)


/*** Lookup customer in View based on order inforamtion if customer exists in system then ERP_Id is set and validated set true otherwise it remains false ***/

IF @AddrType = 'B'
BEGIN
UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
       ,[Bill_Address_ID] = @AddressId
 WHERE [Id] = @Id
END
ELSE IF @AddrType = 'S'
BEGIN
UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
       ,[Ship_Address_ID] = @AddressId
 WHERE [Id] = @Id
END

Return 0
GO
