USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_ValidateTax]

@OrderId nvarchar(20),
@TaxAmount nvarchar(20),
@ShipToZipcode nvarchar(5)

AS

DECLARE @Updated_At datetimeoffset
DECLARE @ValidTaxAmount money
DECLARE @Id int

SET @Updated_At = GETDATE()
SET @ValidTaxAmount = CONVERT(money, @TaxAmount)
SET @Id = CONVERT(int, @OrderId)

/*****UNCOMMENT FOR SET STATIC TAX CODE *****/
/*
UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[Tax_Validated] = 1
	  ,[Total_Tax_Validated] = @ValidTaxAmount
	  ,[Tax_Code] = (select Tax_Code from ERPDefaults where Site_id = 1)
 WHERE [Id] = @Id
*/


/*****UNCOMMENT FOR DYNAMIC TAX CODE *****/
/*** Look up salse tax using lookup table if found returns taxcode assoceated with zipcode
otherwise return default code from defaults table ***/

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[Tax_Validated] = 1
	  ,[Total_Tax_Validated] = @ValidTaxAmount
	  ,[Tax_Code] = (select Case when @ShipToZipcode in (select ZIP from V_TaxCodeLookup) then (select TaxCode from V_TaxCodeLookup where ZIP = @ShipToZipcode) else (select Tax_Code from ERPDefaults where Site_id = 1)end)
 WHERE [Id] = @Id


Return 0
GO
