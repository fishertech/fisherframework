USE $(FWDATABASE)
GO

/* -- USE WHEN REVISING EXISTING STORED PROCEDURE 
DROP PROCEDURE [dbo].[FTC_SP_ManageCategories]
GO
*/




CREATE PROCEDURE [dbo].[FTC_SP_ManageCategories]

@Name nvarchar(100),
@Action nvarchar(1), -- Action values C = Create , U = Update, D = Delete
@Store_Id nvarchar(50) = NULL,
@ERP_ID nvarchar(50),
@Store_URI nvarchar(max) = NULL,
@Description nvarchar(30) = NULL,
@Last_Store_Update datetimeoffset(7) = NULL,
@Last_ERP_Update datetimeoffset(7) = NULL,
@Sync_Status nvarchar(50) = 'Pending'

AS

DECLARE
@Created_At datetimeoffset(7) = GETDATE(),
@Updated_At datetimeoffset(7) = GETDATE(),
@Result integer = 0

IF @Action = 'U' -- UPDATE
	BEGIN
		UPDATE [dbo].[Categories]
		   SET [Store_Id] = case when @Store_Id = NULL THEN (Select Store_Id from Categories where Name = @Name) ELSE @Store_Id END
			  ,[Store_URI] = case when @Store_URI = NULL THEN (Select Store_URI from Categories where Name = @Name) ELSE @Store_URI END
			  ,[Description] = case when @Description = NULL THEN (Select [Description] from Categories where Name = @Name) ELSE @Description END
			  ,[Last_Store_Update] = case when @Last_Store_Update = NULL THEN (Select Last_Store_Update from Categories where Name = @Name) ELSE @Last_Store_Update END
			  ,[Last_ERP_Update] = case when @Last_ERP_Update = NULL THEN (Select Last_ERP_Update from Categories where Name = @Name) ELSE @Last_ERP_Update END
			  ,[Sync_Status] = case when @Sync_Status = NULL THEN (Select Store_Id from Categories where Name = @Name) ELSE @Sync_Status END
			  ,[Updated_At] = @Updated_At
			WHERE Name = @Name
		RETURN
	END

IF @Action = 'C' --CREATE
	BEGIN
		IF EXISTS (SELECT Store_Id from Categories where Store_Id = @Store_Id) 
			RETURN -- Item cannot be created it already exists
		ELSE
		INSERT INTO [dbo].[Categories]
			([Name]
			,[Store_Id]
			,[Store_URI]
			,[Description]
			,[ERP_Id]
			,[Last_Store_Update]
			,[Last_ERP_Update]
			,[Sync_Status]
			,[Created_At]
			,[Updated_At])
		VALUES
			(@Name,
			@Store_Id,
			@Store_URI,
			@Description,
			@ERP_ID,
			@Last_Store_Update,
			@Last_ERP_Update,
			@Sync_Status,
			@Created_At,
			@Updated_At)
		RETURN
	END			

IF @Action = 'D' -- DELETE
BEGIN
	DELETE FROM [dbo].[Categories]
		  WHERE Name = @Name
	RETURN
END