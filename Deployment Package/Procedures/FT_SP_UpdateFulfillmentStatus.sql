USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_UpdateFulfillmentStatus]
--PARAMS
	@Id nvarchar(20),
	@Status nvarchar(20),
    @Updated_At_Overide nvarchar(20) = NULL,
    @Tracking nvarchar(50) = NULL,
    @ERP_Doc_Number nvarchar(20),
    @Shop_Fulfillment_Id nvarchar(20)
AS
 --DECALRED VARS
DECLARE @Updated_At datetime
DECLARE @ID_int as int
DECLARE @Shop_Fmnt_Id_int as int
    
SET @Updated_AT = (SELECT CASE WHEN @Updated_At_Overide is null THEN GETDATE() ELSE CONVERT(datetime,@Updated_At,120) END)
SET @ID_int = CAST(@ID as int)
SET @Shop_Fmnt_Id_int = CAST( @Shop_Fulfillment_Id as int)

IF @Tracking is not null 
BEGIN
INSERT INTO [FT_StagingDB_Shopify].[dbo].[OrderFulfillments]
           ([Order_Id]
           ,[Fullfillment_Id]
           ,[ERP_Document_Number]
           ,[Tracking_Number])
     VALUES
           (@ID_int
           ,@Shop_Fmnt_Id_int
           ,@ERP_Doc_Number
           ,@Tracking)

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[Fulfillment_Status] = @Status
 WHERE [Id] = @ID_int
 
 
END

ELSE 
BEGIN --if the tracking is null then status stays null
UPDATE [dbo].[OrderFulfillments]
   SET [Fullfillment_Id] = @Shop_Fmnt_Id_int
 WHERE [Order_Id] = @ID_int

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[Fulfillment_Status] = @Status
 WHERE [Id] = @ID_int
END
GO
