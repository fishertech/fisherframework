USE $(FWDATABASE)
GO
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql

-- THIS VARIATION SUPPORTS NEW FIELD FOR ITEM LINE NUMBER
-- USING THIS LINE NUMBER FIELD IT IS POSSIBLE TO DETECT AN ITEM THAT IS 
-- ALREADY IN THE FRAMEWORK AND PERFORM AN UPDATE INSTEAD OF A INSERT

CREATE PROCEDURE [dbo].[FT_SP_InsertOrder]
      @Store_Id nvarchar(30),
      @Store_URI nvarchar(max),
      @ERP_Id nvarchar(30),
      @Store_Customer nvarchar(50),
      @ERP_Customer nvarchar(50),
      @Store_Status nvarchar(50),
      @Created_At nvarchar(50),
      @Updated_At nvarchar(50),
      @Note nvarchar(max),
      @Accepts_Marketing bit,
      @Bill_Address_ID nvarchar(30),
      @Bill_First_Name nvarchar(100),
      @Bill_Last_Name nvarchar(100),
      @Bill_Company nvarchar(100),
      @Bill_Address nvarchar(100),
      @Bill_Address2 nvarchar(100),
      @Bill_Address3 nvarchar(100),
      @Bill_City nvarchar(100),
      @Bill_Province_Code nvarchar(3),
      @Bill_Province_Name nvarchar(100),
      @Bill_Post_Code nvarchar(20),
      @Bill_Country_Code nvarchar(3),
      @Bill_Country_Name nvarchar(100),
      @Bill_Latitude nvarchar(20),
      @Bill_Longitude nvarchar(20),
      @Cart_Token nvarchar(100),
      @Email nvarchar(150),
      @Tags nvarchar(max),
      @Pay_Amount money,
      @Pay_Authoirzation nvarchar(100),
      @Pay_Gateway nvarchar(100),
      @Pay_Kind nvarchar(100),
      @Pay_Id int,
      @Pay_Status nvarchar(50),
      @Financial_Status nvarchar(50),
      @Ship_Address_ID nvarchar(100),
      @Ship_First_Name nvarchar(100),
      @Ship_Last_Name nvarchar(100),
      @Ship_Company nvarchar(100),
      @Ship_Address nvarchar(100),
      @Ship_Address2 nvarchar(100),
      @Ship_Address3 nvarchar(100),
      @Ship_City nvarchar(100),
      @Ship_Province_Code nvarchar(3),
      @Ship_Province_Name nvarchar(100),
      @Ship_Post_Code nvarchar(20),
      @Ship_Country_Code nvarchar(3),
      @Ship_Country_Name nvarchar(100),
	  @Ship_Latitude nvarchar(20),
	  @Ship_Longitude nvarchar(20),
      @Fulfillment_Status nvarchar(50),
	  @Freight_Title nvarchar(50),
	  @Freight_Carrier nvarchar(50),
	  @Freight_Code nvarchar(50),
      @Taxes_Included bit,
	  @Item_Id nvarchar(30),
	  @Item_Price money,
	  @Item_Sku nvarchar(50),
	  @Item_Quantity int,
	  @Order_Line_Number nvarchar(30), --new id for store line number
	  @Total_Discount money,
	  @Total_Lines money,
	  @Total_Freight money,
	  @Total_Order money,
	  @Total_Tax money,
	  @Total_Tax_Rate float,
	  @Total_Tax_Validated money,
	  @WebOrderID nvarchar(30),
	  @Bill_Phone nvarchar(20),
	  @Ship_Phone nvarchar(20),
	  @Line_Discount_Percent int,
	  @Line_Discount_Amount money  

AS
  DECLARE @Id int
  DECLARE @NextId int
  DECLARE @Line_Id int
  DECLARE @Sync_Status nvarchar(50)
  DECLARE @Updated_AtTime datetimeoffset
  DECLARE @Created_AtTime datetimeoffset
  DECLARE @Short_Note nvarchar(250)

SET @Short_Note = Left(@Note, 250)   
SET @Sync_Status = 'NEW'
SET @Updated_AtTime = CONVERT(datetimeoffset,@Updated_At,120)
SET @Created_AtTime = CONVERT(datetimeoffset,@Created_At,120)

If @Store_Id NOT IN (Select Store_Id from dbo.Orders)
BEGIN
Set @NextId = (Select case when MAX(Id) IS NULL then 1 else (MAX(Id )+1) end as Next_ID from dbo.Orders)

INSERT INTO [dbo].[Orders]
           ([Id]
           ,[Store_Id]
           ,[Store_URI]
           ,[ERP_Id]
		   ,[Customer_Validated]
           ,[Store_Customer]
           ,[ERP_Customer]
           ,[Store_Status]
           ,[Sync_Status]
           ,[Created_At]
           ,[Updated_At]
           ,[Note]
           ,[Accepts_Marketing]
           ,[WebOrderNumber]
           ,[Bill_Address_ID]
           ,[Bill_First_Name]
           ,[Bill_Last_Name]
           ,[Bill_Company]
           ,[Bill_Address]
           ,[Bill_Address2]
           ,[Bill_Address3]
           ,[Bill_City]
           ,[Bill_Province_Code]
           ,[Bill_Province_Name]
           ,[Bill_Post_Code]
           ,[Bill_Country_Code]
           ,[Bill_Country_Name]
           ,[Bill_Latitude]
           ,[Bill_Longitude]
           ,[Cart_Token]
           ,[Email]
           ,[Tags]
           ,[Pay_Amount]
           ,[Pay_Authoirzation]
           ,[Pay_Gateway]
           ,[Pay_Kind]
           ,[Pay_Id]
           ,[Pay_Status]
           ,[Financial_Status]
           ,[Taxes_Included]
           ,[Ship_Address_ID]
           ,[Ship_First_Name]
           ,[Ship_Last_Name]
           ,[Ship_Company]
           ,[Ship_Address]
           ,[Ship_Address2]
           ,[Ship_Address3]
           ,[Ship_City]
           ,[Ship_Province_Code]
           ,[Ship_Province_Name]
           ,[Ship_Post_Code]
           ,[Ship_Country_Code]
           ,[Ship_Country_Name]
	       ,[Ship_Latitude]
	       ,[Ship_Longitude]
		   ,[Store_Ship_Code]
		   ,[Store_Ship_Carrier]
		   ,[Store_Ship_Method]
		   ,[ERP_Ship_Method]
		   ,[Freight_Validated]
           ,[Fulfillment_Status]
	       ,[Total_Discount]
	       ,[Total_Lines]
		   ,[Total_Freight]
	       ,[Total_Order]
	       ,[Total_Tax]
		   ,[Total_Tax_Rate]
	       ,[Total_Tax_Validated]
	       ,[VatExemption] 
	       ,[Bill_Phone]
	       ,[Ship_Phone])
     VALUES
           (@NextId
           ,@Store_Id
           ,@Store_URI
           ,@ERP_Id
		   ,(select case when (Select top 1 'true' as [result] from V_CustomerLookup where Store_Id = @Store_Customer) = 'true' then 1 else 0 end)
           ,@Store_Customer
           ,(Select top 1 ERP_Id from V_CustomerLookup where Store_Id = @Store_Customer)
           ,@Store_Status
           ,@Sync_Status
           ,@Created_AtTime
           ,@Updated_AtTime
           ,@Short_Note
           ,@Accepts_Marketing
           ,@WebOrderID
           ,@Bill_Address_ID
           ,@Bill_First_Name
           ,@Bill_Last_Name
           ,@Bill_Company
           ,@Bill_Address
           ,@Bill_Address2
           ,@Bill_Address3
           ,@Bill_City
           ,@Bill_Province_Code
           ,@Bill_Province_Name
           ,@Bill_Post_Code
           ,@Bill_Country_Code
           ,@Bill_Country_Name
           ,@Bill_Latitude
           ,@Bill_Longitude
           ,@Cart_Token
           ,@Email
           ,@Tags
           ,@Pay_Amount
           ,@Pay_Authoirzation
           ,@Pay_Gateway
           ,@Pay_Kind
           ,@Pay_Id
           ,@Pay_Status
           ,@Financial_Status
           ,@Taxes_Included
           ,@Ship_Address_ID
           ,@Ship_First_Name
           ,@Ship_Last_Name
           ,@Ship_Company
           ,@Ship_Address
           ,@Ship_Address2
           ,@Ship_Address3
           ,@Ship_City
           ,@Ship_Province_Code
           ,@Ship_Province_Name
           ,@Ship_Post_Code
           ,@Ship_Country_Code
           ,@Ship_Country_Name
	       ,@Ship_Latitude
	       ,@Ship_Longitude
		   ,@Freight_Code
		   ,@Freight_Carrier
		   ,@Freight_Title
		   ,(SELECT top 1 ERPMethod FROM V_FreightLookup WHERE ShopMethod = @Freight_Title)
		   ,(SELECT CASE WHEN (SELECT 'true' AS [result] FROM V_FreightLookup WHERE ShopMethod = @Freight_Title) = 'true' THEN 1 ELSE 0 END)
           ,@Fulfillment_Status
	       ,@Total_Discount
	       ,@Total_Lines
		   ,@Total_Freight
	       ,@Total_Order
	       ,@Total_Tax
		   ,@Total_Tax_Rate
	       ,@Total_Tax_Validated
	       ,(SELECT top 1 Exempt FROM V_CustomerLookup WHERE Store_Id = @Store_Customer )
	       ,@Bill_Phone
	       ,@Ship_Phone)
END

--Enter line information
BEGIN

Set @Id = (Select DISTINCT Id from dbo.Orders where Store_Id = @Store_Id and Store_URI = @Store_URI )
Set @Line_Id = (Select DISTINCT COUNT(*) + 1 as Next_ID from dbo.OrderLines)

If @Order_Line_Number NOT IN (Select [Order_Line_Number] from dbo.OrderLines where ID = @Id AND Order_Line_Number = @Order_Line_Number) --CHECK IF THE ITEM LINE IS ALREADY IN FW
	BEGIN
		INSERT INTO [dbo].[OrderLines]
				   ([Id]
				   ,[Line_Id]
				   ,[Item_Id]
				   ,[Item_Price]
				   ,[Item_Sku]
				   ,[Item_Quantity]
				   ,[Order_Line_Number]
				   ,[ERP_Id]
				   ,[Sync_Status]
				   ,[Created_At]
				   ,[Updated_At]
				   ,[Taxes_Included]
				   ,[Fulfillment_Status]
				   ,[Line_Discount_Percent]
				   ,[Line_Discount_Amount])
			 VALUES
				   (@Id
				   ,@Line_Id
				   ,@Item_Id
				   ,@Item_Price
				   ,@Item_Sku
				   ,@Item_Quantity
				   ,@Order_Line_Number -- NEW REFERENCE TO STORE LINE
				   ,(select top 1 ERP_Id from V_ItemLookup where Store_Id = @Item_Sku)
				   ,@Sync_Status
				   ,@Created_AtTime
				   ,@Updated_AtTime
				   ,@Taxes_Included
				   ,@Fulfillment_Status
				   ,@Line_Discount_Percent
				   ,@Line_Discount_Amount)

	END
ELSE
	BEGIN
		-- THE LINE ALREADY EXISTS IN THE DATABASE SO PERFORM AN UPDATE ON THE ITEM INSTEAD
		SET @Sync_Status = 'UPDATED'

		UPDATE [dbo].[OrderLines]
		   SET [Item_Id] = @Item_Id
			  ,[Item_Price] = @Item_Price
			  ,[Item_Sku] = @Item_Sku
			  ,[Item_Quantity] = @Item_Quantity
			  ,[Sync_Status] = @Sync_Status
			  ,[Updated_At] = @Updated_At
			  ,[Taxes_Included] = @Taxes_Included
			  ,[Fulfillment_Status] = @Fulfillment_Status
			  ,[Line_Discount_Percent] = @Line_Discount_Percent
			  ,[Line_Discount_Amount] = @Line_Discount_Amount
		 WHERE Id = @Id AND Order_Line_Number = @Order_Line_Number
	END

RETURN 0 END
GO