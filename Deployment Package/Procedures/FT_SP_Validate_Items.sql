USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_Validate_Items]
--PARAMS
--	@ID int,
--    @Updated_At_Overide nvarchar(20)
AS
--DECALRED VARS
DECLARE @Updated_At datetime

select id, COUNT(*) as NumLines into #NumLines
from orderlines
group by id

select id, COUNT(*) as NumLinesValid into #NumLinesX
from orderlines
where erp_id is not null
group by id

SET @Updated_AT = GETDATE()

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At	
	  ,[items_validated] = 1
where sync_status='New' and items_validated=0
and ID in (
	select a.ID from #NumLines a,#NumLinesX b
	where a.Id=b.id and NumLines=NumLinesValid
	)

drop table #numlines
drop table #numlinesx
GO


