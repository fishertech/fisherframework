USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE PROCEDURE [dbo].[FT_SP_UpdateFreight]
--PARAMS
	@ID nvarchar(20),
    @Updated_At_Overide nvarchar(20)
AS
 --DECALRED VARS
DECLARE @Updated_At datetime
DECLARE @ID_int as int
DECLARE @Freight_Method nvarchar(50)
DECLARE @ERP_Method nvarchar(50)
DECLARE @Freight_Validated nvarchar(1)

SET @Updated_AT = (SELECT CASE WHEN @Updated_At_Overide is null THEN GETDATE() ELSE CONVERT(datetime,@Updated_At,120) END)
SET @ERP_Method = (SELECT ERP_Ship_Method FROM Orders WHERE Id = @ID)
SET @Freight_Method = (SELECT Store_Ship_Method FROM Orders WHERE Id = @ID)
SET @ERP_Method = (SELECT CASE WHEN @ERP_Method is null THEN (SELECT top 1 ERPMethod FROM V_FreightLookup WHERE ShopMethod = @Freight_Method) ELSE @ERP_Method END)
SET @Freight_Validated = (SELECT CASE WHEN @ERP_Method is null THEN '0' ELSE '1' END)
SET @ID_int = CAST(@ID as int)

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[ERP_Ship_Method] = @ERP_Method
      ,[Freight_Validated] = @Freight_Validated
 WHERE [Id] = @Id
GO
