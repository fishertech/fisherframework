USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sq
GO

CREATE PROCEDURE [dbo].[FT_SP_UpdateSyncStatus]
--PARAMS
	@Id nvarchar(20),
	@Status nvarchar(20),
    @Updated_At_Overide nvarchar(20),
    @ERP_ID nvarchar(20)
AS
 --DECALRED VARS
DECLARE @Updated_At datetime
DECLARE @ID_int as int
DECLARE @ERP_ID_Int as int
    
SET @Updated_AT = (SELECT CASE WHEN @Updated_At_Overide is null THEN GETDATE() ELSE CONVERT(datetime,@Updated_At,120) END)
SET @ID_int = CAST(@ID as int)
SET @ERP_ID_Int = CAST(@ERP_ID as int)

UPDATE [dbo].[Orders]
   SET [Updated_At] = @Updated_At
      ,[Sync_Status] = @Status
      ,[ERP_Id] = @ERP_ID_Int
 WHERE [Id] = @ID_int
GO
