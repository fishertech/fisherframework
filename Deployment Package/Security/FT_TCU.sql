USE [master]
GO
-- CLEANLY REMOVE TaskCentre USER FROM ALL ROLLS AND DATABASES
EXEC sp_Msforeachdb 'IF EXISTS(SELECT * FROM sys.database_principals WHERE name = N''TaskCentre'') DROP USER TaskCentre' 
--EXEC sp_Msforeachdb "Select * from sys.database_principals WHERE name = N'TaskCentre'" 
GO
IF EXISTS (SELECT * FROM sys.server_principals WHERE name = N'TaskCentre') DROP LOGIN [TaskCentre]  
GO

-- CREATE TaskCentre USER
CREATE LOGIN [TaskCentre] WITH PASSWORD=0x0100E3A47F6730A1BA80B64BB301E7A32E826158DF8F3D608646 HASHED, DEFAULT_DATABASE=[master], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
ALTER SERVER ROLE [sysadmin] ADD MEMBER [TaskCentre]
GO
USE [$(FWDATABASE)]
IF NOT EXISTS (select * from sys.database_principals where name = 'TaskCentre')
BEGIN
    PRINT 'CREATE LOGIN FOR $(FWDATABASE)'
	CREATE USER [TaskCentre] FOR LOGIN [TaskCentre]
END
ELSE
	PRINT 'USER EXISTS IN $(FWDATABASE)'
ALTER USER [TaskCentre] WITH DEFAULT_SCHEMA=[dbo]
ALTER ROLE [db_owner] ADD MEMBER [TaskCentre]
GO

USE [$(COMMONDB)]
IF NOT EXISTS (select * from sys.database_principals where name = 'TaskCentre')
BEGIN
	PRINT 'CREATE LOGIN FOR $(COMMONDB)'  
	CREATE USER [TaskCentre] FOR LOGIN [TaskCentre]
END
ELSE
	PRINT 'USER EXISTS IN $(COMMONDB)'
ALTER USER [TaskCentre] WITH DEFAULT_SCHEMA=[dbo]
ALTER ROLE [db_owner] ADD MEMBER [TaskCentre]
GO

USE [$(TARGETDB)]
IF NOT EXISTS (select * from sys.database_principals where name = 'TaskCentre')
BEGIN
	PRINT 'CREATE LOGIN FOR $(TARGETDB)'	  
	CREATE USER [TaskCentre] FOR LOGIN [TaskCentre]
END
ELSE
	PRINT 'USER EXISTS IN $(TARGETDB)'
ALTER USER [TaskCentre] WITH DEFAULT_SCHEMA=[dbo]
ALTER ROLE [db_owner] ADD MEMBER [TaskCentre]
GO
