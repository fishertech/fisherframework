USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE VIEW [dbo].[V_UnfulfilledOrders]
as
select T0.DocEntry as U_BaseEntry,T0.docnum as U_BaseNum, 15 as U_BaseType, T0.TrackNo as U_MstrTrck,T0.$(MarketingDocWebIDColumn), T0.DocNum, T2.Id
from $(TARGETDB).dbo.ODLN T0
join $(FWDATABASE).dbo.Orders  T2 on T2.Store_Id = $(MarketingDocWebIDColumn)  COLLATE SQL_Latin1_General_CP1_CI_AS
where  len(T0.TrackNo)> 1
   AND T2.Fulfillment_Status is null

GO