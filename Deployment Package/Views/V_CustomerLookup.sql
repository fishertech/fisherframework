USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE VIEW [dbo].[V_CustomerLookup]
as
--/* UNCOMMENT THE BLOCK BELOW WHEN BUSINESS PARTNERS ARE CREATED FOR EACH WEB CUSTOMER */
--/*
SELECT        $(PartnerWebIDColumn) AS Store_Id, CardCode AS ERP_Id, CASE WHEN VatStatus = 'N' THEN 1 ELSE 0 END AS Exempt
FROM            $(TARGETDB).dbo.OCRD
WHERE        ($(PartnerWebIDColumn) IS NOT NULL) AND ($(PartnerWebIDColumn) <> '')
GO
--*/

--/* UNCOMMENT THE BLOCK BELOW WHEN BUSINESS PARTNERS BUCKET ACCOUNT IS BEING USED */
/*
SELECT       Distinct T1.Store_Id AS Store_Id, T2.CardCode AS ERP_Id, CASE WHEN T2.VatStatus = 'N' THEN 1 ELSE 0 END AS Exempt
FROM            $(FWDATABASE).dbo.Orders T1
        JOIN (Select Bucket_BPCode as CardCode, Bucket_Vat_Exemption as VatStatus from ERPDefaults where Site_id = 1) T2 on 1=1
GO
--*/