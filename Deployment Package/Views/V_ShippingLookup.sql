USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE VIEW [dbo].[V_ShippingLookup]
AS
SELECT  T0.DocEntry, 
        T0.DocNum, 
        T0.CardCode, 
        T0.CardName, 
        T0.NumAtCard,
        T0.DocDate, 
        T0.DocDueDate, 
        T0.DocTotal, 
        T0.TrackNo, 
        T0.$(MarketingDocWebIDColumn), 
        T3.Id AS RefID
FROM            $(TARGETDB).dbo.ODLN AS T0 LEFT OUTER JOIN
                         $(FWDATABASE).dbo.Orders AS T3 ON T3.Store_Id = T0.$(MarketingDocWebIDColumn) COLLATE SQL_Latin1_General_CP1_CI_AS
WHERE        (T0.$(MarketingDocWebIDColumn) IS NOT NULL)
GO
