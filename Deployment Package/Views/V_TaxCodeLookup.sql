USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE VIEW [dbo].[V_TaxCodeLookup]
as
-- UNCOMMENT LINE BELOW if using USER DEFINED TAX TABLE
/*	 
select * from $(TARGETDB).dbo.[@SALESTAX]
--*/
--/*  COMMENT THIS CODE WHEN USING USER DEFINED TAX TABLE
select distinct left(ZipCode,5) as ZIP,TaxCode,Name, Rate from $(TARGETDB).dbo.crd1
join $(TARGETDB).dbo.ostc on taxcode=Code
where AdresType='S' 
and TaxCode is not null
and ZipCode is not null

--*/
GO
