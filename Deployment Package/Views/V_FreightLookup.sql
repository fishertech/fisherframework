USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE VIEW [dbo].[V_FreightLookup]
AS

--/*Use this method to return values from internal database when needed */
/*
select Store_Code As ShopMethod, 
       Erp_Code As ERPMethod, 
       [Description] As Description
from $(FWDATABASE).[dbo].FreightMapping
--*/


--/*Use this method to return values as-is from the ERP Database */
--/*
SELECT  TrnspName AS ShopMethod, 
        TrnspCode AS ERPMethod, 
        TrnspName As Description
FROM    $(TARGETDB).dbo.OSHP
GO
--*/