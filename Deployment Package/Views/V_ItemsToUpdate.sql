USE $(FWDATABASE)
GO

CREATE VIEW [dbo].[V_ItemsToUpdate]
AS
SELECT 
		T1.ItemCode as 'SKU',
		'staging.dovetale.com' as 'storeURI',
		T3.Store_Id as 'Store_Id', 
		T1.ItemCode as 'ERP_ID', 
		'ERP' as 'SyncSource', 
		cast(T1.UpdateDate as datetimeoffset(7)) as 'UpdateDate', 
		T3.Last_ERP_Sync as 'Last_ERP_Sync', 
		(select case when cast(t1.OnHand + T1.OnOrder - T1.IsCommited as decimal(16,0)) <> ISNULL(Stock_Value, 0) then 'Changed' else 'Current' end) as 'Stock_Status', 
		ISNULL(Stock_Value,0) as 'Stock_History',
		cast(t1.OnHand + T1.OnOrder - T1.IsCommited as decimal(16,0)) as 'Current_Stock',
		ISNULL(Revision,0) as 'Revision_History', 
		ERP_Revision as 'ERP_Revision', 
		(select Case when ERP_Revision > ISNULL(Revision, 0)  Then 'Revised' else 'Current' end ) as 'Revision_Status'
FROM $(TARGETDB).dbo.OITM T1
left join $(TARGETDB).dbo.OMRC T2 on T1.FirmCode = T2.FirmCode
left join $(FWDATABASE).dbo.Items T3 on T1.ItemCode = T3.ERP_Id collate SQL_Latin1_General_CP1_CI_AS
left join (select ItemCode, count(itemcode) as 'ERP_Revision' from $(TARGETDB).dbo.aitm group by ItemCode) T4 on T1.ItemCode = T4.ItemCode
where QryGroup12='Y' --Customer Specific Flag