USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

CREATE VIEW [dbo].[V_NextCustomerCode]
as
select 'C' +  CAST(tmp.nextno as nvarchar(20)) as nextcardcode from (
Select Max(convert(int,Right(custcards.CardCode,LEN(cardcode)-1)))+1 as nextno  From
(select * from $(TARGETDB).dbo.OCRD where left(CardCode,1) = 'C')custcards) tmp
GO