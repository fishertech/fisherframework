USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE VIEW [dbo].[V_OrderAddresses]
as
SELECT     headinfo.Id, addresses.Address_ID, addresses.type, addresses.simpletype, addresses.First_Name, addresses.Last_Name, addresses.Full_Name, 
                      addresses."Address", addresses.Address2, addresses.Address3, addresses.City, addresses.Province, addresses.Zipcode, addresses.Phone, 
                      addresses.Company, addresses.Country_Code, headinfo.StoreCustomerID, headinfo.BPName, headinfo.Email, addresses.Customer_Validated, 
                      headinfo.ERP_Customer, headinfo.nextBPCode
FROM        (SELECT 
				Id, 
				'bo_BillTo' AS type, 'b' AS simpletype, 
				Bill_Address_ID AS Address_ID, 
				Bill_First_Name AS First_Name, 
				Bill_Last_Name AS Last_Name, 
                Bill_First_Name + ' ' + Bill_Last_Name AS Full_Name, 
				Bill_Address AS "Address", 
				Bill_Address2 AS Address2, 
				Bill_Address3 AS Address3, 
                Bill_City AS City, 
				Bill_Phone AS Phone, 
				Bill_Province_Code AS Province, 
				Bill_Post_Code AS Zipcode, 
				CASE WHEN Bill_Company IS NULL THEN (Bill_First_Name + ' ' + Bill_Last_Name) ELSE Bill_Company END AS Company, 
				Bill_Country_Code AS Country_Code, 
				Customer_Validated
			FROM  $(FWDATABASE).dbo.Orders
		UNION
			SELECT
				Id, 
				'bo_ShipTo' AS type, 
				's' AS simpletype, 
				Ship_Address_ID AS Address_ID, 
				Ship_First_Name AS First_Name, 
				Ship_Last_Name AS Last_Name, 
				Ship_First_Name + ' ' + Ship_Last_Name AS Full_Name, 
				Ship_Address AS "Address", 
				Ship_Address2 AS Address2, 
				Ship_Address3 AS Address3, 
				Ship_City AS City, 
				Ship_Phone AS Phone, 
				Ship_Province_Code AS Province, 
				Ship_Post_Code AS Zipcode, 
				CASE WHEN Ship_Company IS NULL THEN (Ship_First_Name + ' ' + Ship_Last_Name) ELSE Ship_Company END AS Company, 
				Ship_Country_Code AS Country_Code,
				Customer_Validated
			FROM $(FWDATABASE).dbo.Orders) AS addresses 
			--UNION SPLITS SHIP AND BILLING ADDRESSES INTO SEPARATE RECORDS TO BE PROCESSED SEPARATLY
		LEFT OUTER JOIN
			--JOIN HEADER DATA TO EACH ADDRESS LINE TO DENORMALIZE THE DATA
			(SELECT	Id, 
				Store_Customer AS StoreCustomerID, 
				Bill_First_Name + ' ' + Bill_Last_Name AS BPName, 
				Email, 
				ERP_Customer,
				(SELECT nextcardcode FROM $(FWDATABASE).dbo.V_NextCustomerCode) AS nextBPCode
			FROM    $(FWDATABASE).dbo.Orders) AS headinfo 
		ON headinfo.Id = addresses.Id
GO