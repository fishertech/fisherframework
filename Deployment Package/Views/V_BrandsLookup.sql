USE $(FWDATABASE)
GO
--This view returns a list of Brands currently in the framework and indicates their status
-- Active indicates the Brand is in the framework and uptodate
-- Update indicates the Brand in the Framework but the name differes from ERP
-- New indicates the Brand is not in the Framework

Select TA.FirmCode as 'ERP_ID', 
	   TA.FirmName as 'Name', 
	   IsNull(TB.Status, 'Active') as 'Status', 
	   TC.Store_Id as 'Store_Id',
	   TC.Last_ERP_Update,
	   TC.Last_Store_Update,
	   TC.Updated_At,
	   (Case when Last_Store_Update is null or Last_Store_Update < Last_ERP_Update Then 'Update_Store' Else 'Complete' End) as Store_Status
from $(TARGETDB).dbo.OMRC TA 
Left Join 
(Select T0.FirmCode as 'ERP_ID', 'Update' as 'Status' from $(TARGETDB).dbo.OMRC T0 , (select ERP_ID, Name from FT_Framework.dbo.Brands) T1 where FirmCode = ERP_Id collate SQL_Latin1_General_CP850_CI_AS AND FirmName <> Name  collate SQL_Latin1_General_CP850_CI_AS
Union
Select T0.FirmCode as 'ERP_ID', 'New' as 'Status' from $(TARGETDB).dbo.OMRC T0 where FirmCode > 0 and FirmCode not IN ( Select ERP_ID from FT_Framework.dbo.Brands)) TB 
ON TA.FirmCode = TB.ERP_ID
Left Join Brands TC on TC.ERP_Id = TA.FirmCode
where TA.FirmCode > 0