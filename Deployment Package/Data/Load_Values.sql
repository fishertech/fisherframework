USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO

INSERT INTO [ERPDefaults]
           ([Site_id]
           ,[Order_CCEV]
           ,[Order_Confirmed]
           ,[Order_Installments]
           ,[Order_OwnerCode]
           ,[Order_PayType]
           ,[Order_TaxStatus]
           ,[Order_TaxType]
           ,[Order_ExpenseCode]
           ,[Tax_Code]
           ,[Tax_Offset_Code]
		   ,[Allow_Test_Orders]
           ,[Bucket_BPCode]
           ,[Bucket_Vat_Exemption])
     VALUES
           (1,'RM','tYES',1,1,'CC','tYES','tt_Yes',1,0,0,0,'','N')
GO         
INSERT INTO [dbo].[TestUsers]
           ([Name],[Email],[StoreID],[ERPID])
     VALUES
           ('SAP Test East','saptesteast@fisher-technology.com','3474889092','C061118'),
           ('SAP Test West','saptestwest@fisher-technology.com','3474904196','C061121'),
           ('SAP Test Mid West','saptestmidwest@fisher-technology.com','3474912580','C061122'),
           ('SAP Test North East','saptestnortheast@fisher-technology.com','3474930052','C061123');
GO
