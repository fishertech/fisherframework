USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE TABLE [dbo].[OrderLines](
	[Id] [int] NOT NULL,
	[Line_Id] [int] NOT NULL,
	[Item_Id] [nvarchar](30) NULL,
	[Item_Price] [money] NULL DEFAULT ((0)),
	[Item_Sku] [nvarchar](50) NOT NULL,
	[Item_Quantity] [int] NOT NULL,
	[Order_Line_Number] [nvarchar](30) NOT NULL,
	[ERP_Id] [nvarchar](30) NULL DEFAULT (''),
	[Sync_Status] [nvarchar](50) NULL DEFAULT ('NEW'),
	[Created_At] [datetimeoffset](7) NULL DEFAULT (getdate()),
	[Updated_At] [datetimeoffset](7) NULL DEFAULT (getdate()),
	[Taxes_Included] [bit] NULL DEFAULT ((0)),
	[Fulfillment_Status] [nvarchar](50) NULL DEFAULT (''),
	[Line_Discount_Percent] [float] NULL,
	[Line_Discount_Amount] [money] NULL,
 CONSTRAINT [PK_OrderLines] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[Line_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO