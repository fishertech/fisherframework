USE $(FWDATABASE)
GO
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
--DROP TABLE [dbo].[Items]
GO


CREATE TABLE [dbo].[Items](
	[Sku] [nvarchar](100) NOT NULL,
	[Store_Id] [nvarchar](30) NULL,
	[Store_URI] [nvarchar](max) NULL,
	[ERP_Id] [nvarchar](30) NULL,
	[Store_Status] [nvarchar](50) NULL,
	[Sync_Status] [nvarchar](50) NULL,
	[Created_At] [datetimeoffset](7) NULL,
	[Updated_At] [datetimeoffset](7) NULL,
	[Last_Store_Sync] [datetimeoffset](7) NULL,
	[Last_ERP_Sync] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_Items] PRIMARY KEY CLUSTERED 
(
	[Sku] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Items] ADD  DEFAULT (GETDATE()) FOR [Created_At]
GO

ALTER TABLE [dbo].[Items] ADD  DEFAULT (GETDATE()) FOR [Updated_At]
GO

ALTER TABLE [dbo].[Items] ADD  DEFAULT (GETDATE()) FOR [Last_Store_Sync]
GO

ALTER TABLE [dbo].[Items] ADD  DEFAULT (GETDATE()) FOR [Last_ERP_Sync]
GO




