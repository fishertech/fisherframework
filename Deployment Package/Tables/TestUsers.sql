USE $(FWDATABASE)
GO

CREATE TABLE [dbo].[TestUsers](
	[Name] nvarchar(150),
	[Email] nvarchar(200),
	[StoreID] nvarchar(50),
	[ERPID] nvarchar(50))

GO
