USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE TABLE [dbo].[OrderTransactions](
	[Id] [bigint] NOT NULL,
	[Transaction_Id] [bigint] NOT NULL,
	[Action_Type] [nvarchar](50) NULL,
	[Amount] [money] NULL,
	[Authorization] [nvarchar](50) NULL,
	[Created_At] [datetimeoffset](7) NULL,
	[Gateway] [nvarchar](30) NULL,
	[Kind] [nvarchar](30) NULL,
	[Updated_At] [datetimeoffset](7) NULL,
	[Reference_Code] [nvarchar](100) NULL,
	[Location_id] [int] NULL,
	[Message] [nvarchar](max) NULL,
	[Store_Id] [bigint] NULL,
	[Status] [nvarchar](30) NULL,
	[Test] [nvarchar](10) NULL,
	[User] [int] NULL,
	[Device_id] [nvarchar](50) NULL,
	[PayCardRef] [nvarchar](4) NULL,
	[PayCardCo] [nvarchar](20) NULL,
	[CC_Last4] [int] NULL,
	[CC_ExpireMonth] [int] NULL,
	[CC_ExpireYear] [int] NULL,
 CONSTRAINT [PK_OrderTransactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC,
	[Transaction_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
