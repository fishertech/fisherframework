USE $(FWDATABASE)
GO
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
--DROP TABLE [dbo].[Brands]
GO


CREATE TABLE [dbo].[Brands](
	[Name] [nvarchar](100) NOT NULL,
	[Store_Id] [nvarchar] (50) NULL,
	[Store_URI] [nvarchar](max) NULL,
    [Country] [nvarchar](2) NULL,
	[Description] [nvarchar](30) NULL,
    [ERP_Id] [nvarchar](50) NOT NULL,
	[Last_Store_Update] [datetimeoffset](7) NULL,
	[Last_ERP_Update] [datetimeoffset](7) NULL,
	[Sync_Status] [nvarchar](50) NULL,
	[Created_At] [datetimeoffset](7) NULL,
	[Updated_At] [datetimeoffset](7) NULL,
 CONSTRAINT [PK_Brands] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[Brands] ADD  DEFAULT (GETDATE()) FOR [Created_At]
GO

ALTER TABLE [dbo].[Brands] ADD  DEFAULT (GETDATE()) FOR [Updated_At]
GO
