USE $(FWDATABASE)
--BUILD SCRIPT DEFINED TO RUN AS PART OF BATCH SEE FW_BUILD.sql
GO
CREATE TABLE [dbo].[ERPDefaults](
	[Site_id] [int] NOT NULL,
	[Order_CCEV] [nvarchar](10) NULL,
	[Order_Confirmed] [nvarchar](10) NULL,
	[Order_Installments] [nvarchar](10) NULL,
	[Order_OwnerCode] [nvarchar](10) NULL,
	[Order_PayType] [nvarchar](10) NULL,
	[Order_TaxStatus] [nvarchar](10) NULL,
	[Order_TaxType] [nvarchar](10) NULL,
	[Order_ExpenseCode] [nvarchar](10) NULL,
	[Tax_Code] [nvarchar](20) NULL,
	[Tax_Offset_Code] [nvarchar](20) NULL,
	[Allow_Test_Orders] [varchar](1) NOT NULL,
	[Bucket_BPCode] [nvarchar](20) NULL,
	[Bucket_Vat_Exemption] [nvarchar](1) NULL, -- values Y or N
 CONSTRAINT [PK_ERPDefaults] PRIMARY KEY CLUSTERED 
(
	[Site_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
