use master

DECLARE @pswd NVARCHAR(MAX); SET @pswd = 'T@$kC3ntr3';
DECLARE @salt VARBINARY(4); SET @salt = CAST(NEWID() AS VARBINARY(4));
DECLARE @hash VARBINARY(MAX);
SET @hash = 0x0100 + @salt +
            HASHBYTES('SHA1', CAST(@pswd AS VARBINARY(MAX)) + @salt);
SELECT @hash AS HashValue, PWDCOMPARE(@pswd,@hash) AS IsPasswordHash;