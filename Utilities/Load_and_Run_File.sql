
DECLARE @StagingDB NVARCHAR(50) 
DECLARE @query	NVARCHAR(MAX)=''
DECLARE @FrameworkPATH NVARCHAR(MAX)=''
DECLARE @FileBLOB NVARCHAR(MAX)=''
DECLARE @ParamDef NVARCHAR(500)
DECLARE @FileName NVARCHAR(200)

SET @StagingDB = N'FT_Deployment_Test'
SET @FrameworkPATH = N'C:\Integration Framework\Deployment Package\'
SET @FileName = N'Build_Views.sql'
SET @ParamDef = N'@FileBLOB VARCHAR(MAX) OUTPUT'

SET @query = N'Select @FileBLOB = BulkColumn FROM   OPENROWSET(BULK ''' + @FrameworkPATH + @FileName + ''',SINGLE_BLOB) x;'
EXECUTE sp_executesql @query, @ParamDef, @FileBLOB = @FileBLOB OUTPUT
--PRINT @FileBLOB

SET @query = REPLACE(@FileBLOB, '{DBNAME}', @StagingDB)
PRINT @query
--RUN THE CURRENT QUERY THAT IS LOADED
--EXEC(@query) 