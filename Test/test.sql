use $(FWDATABASE)
go

CREATE VIEW [dbo].[V_CustomerLookup]
as
SELECT        U_WebID AS Store_Id, CardCode AS ERP_Id, CASE WHEN VatStatus = 'N' THEN 1 ELSE 0 END AS Exempt
FROM            $(TARGETDB).dbo.OCRD
WHERE        (U_WebID IS NOT NULL) AND (U_WebID <> '')