DECLARE @RC int
DECLARE @Name nvarchar (100)
DECLARE @Action nvarchar (1)
DECLARE @Store_Id nvarchar(50)
DECLARE @Store_URI nvarchar(max)
DECLARE @Description nvarchar(30)
DECLARE @Last_Store_Update datetimeoffset(7)
DECLARE @Last_ERP_Update datetimeoffset(7)
DECLARE @Sync_Status nvarchar(50)

SET @Name = 'TEST'
SET @Action = 'C' --CREATE


EXECUTE @RC = [dbo].[FTC_SP_ManageCategories] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status
   
Select * from Categories where Name = 'TEST'
WAITFOR DELAY '00:00:02'; -- MAKES THE UPDATE VISABLE ON TIME STAMP
SET @Action = 'U' -- UPDATE
SET @Store_Id = 1001


EXECUTE @RC = [dbo].[FTC_SP_ManageCategories] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status

Select * from Categories where Name = 'TEST'

SET @Action = 'D' --DELETE

EXECUTE @RC = [dbo].[FTC_SP_ManageCategories] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status

Select * from Categories where Name = 'TEST'

-- WHEN FINISHED RECORD IS ADDED, UPDATED AND DELETED LEAVING NO DATA IN FW-DB