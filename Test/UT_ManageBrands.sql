use FT_Framework
go

DECLARE @RC int
DECLARE @Name nvarchar (100)
DECLARE @Action nvarchar (1)
DECLARE @Store_Id nvarchar(50)
DECLARE @Store_URI nvarchar(max)
DECLARE @Country_Code nvarchar(2)
DECLARE @Description nvarchar(30)
DECLARE @Last_Store_Update datetimeoffset(7)
DECLARE @Last_ERP_Update datetimeoffset(7)
DECLARE @Sync_Status nvarchar(50)

SET @Name = 'TEST'
SET @Action = 'C' --CREATE
SET @Last_ERP_Update = GETDATE()
SET @Store_URI = 'somesite.com'
SET @Sync_Status = 'created'


EXECUTE @RC = [dbo].[FTC_SP_ManageBrands] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status
  ,@Country_Code
   
Select * from Brands where Name = 'TEST'
WAITFOR DELAY '00:00:02'; -- MAKES THE UPDATE VISABLE ON TIME STAMP
SET @Action = 'U' -- UPDATE
SET @Store_Id = 1001
SET @Country_Code = 'US'
SET @Description = 'This is a Test'
SET @Last_ERP_Update = NULL
SET @Store_URI = NULL
SET @Last_Store_Update = GETDATE()
SET @Sync_Status = 'updated'


EXECUTE @RC = [dbo].[FTC_SP_ManageBrands] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status
  ,@Country_Code

Select * from Brands where Name = 'TEST'

SET @Action = 'D' --DELETE

EXECUTE @RC = [dbo].[FTC_SP_ManageBrands] 
   @Name
  ,@Action
  ,@Store_Id
  ,@Store_URI
  ,@Description
  ,@Last_Store_Update
  ,@Last_ERP_Update
  ,@Sync_Status
  ,@Country_Code

Select * from Brands where Name = 'TEST'

-- WHEN FINISHED RECORD IS ADDED, UPDATED AND DELETED LEAVING NO DATA IN FW-DB