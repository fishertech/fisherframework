USE [FT_Deployment_Script_Test]
GO

DECLARE @RC int
DECLARE @Store_Id bigint
SET @Store_Id = '99000356'
DECLARE @Store_URI nvarchar(max)
SET @Store_URI = 'rich.myshopify.com'
DECLARE @ERP_Id nvarchar(30)
SET @ERP_Id = '10032'
DECLARE @Store_Customer nvarchar(50)
SET @Store_Customer = '30002893'
DECLARE @ERP_Customer nvarchar(50)
DECLARE @Store_Status nvarchar(50)
DECLARE @Created_At nvarchar(50)
DECLARE @Updated_At nvarchar(50)
DECLARE @Note nvarchar(max)
SET @Note = 'This is a simple note for the system'
DECLARE @Accepts_Marketing bit
SET @Accepts_Marketing = 1
DECLARE @Bill_Address_ID nvarchar(30)
DECLARE @Bill_First_Name nvarchar(100)
DECLARE @Bill_Last_Name nvarchar(100)
DECLARE @Bill_Company nvarchar(100)
DECLARE @Bill_Address nvarchar(100)
DECLARE @Bill_Address2 nvarchar(100)
DECLARE @Bill_Address3 nvarchar(100)
DECLARE @Bill_City nvarchar(100)
DECLARE @Bill_Province_Code nvarchar(3)
DECLARE @Bill_Province_Name nvarchar(100)
DECLARE @Bill_Post_Code nvarchar(20)
DECLARE @Bill_Country_Code nvarchar(3)
DECLARE @Bill_Country_Name nvarchar(100)
DECLARE @Bill_Latitude nvarchar(20)
DECLARE @Bill_Longitude nvarchar(20)
DECLARE @Cart_Token nvarchar(100)
DECLARE @Email nvarchar(150)
DECLARE @Tags nvarchar(max)
DECLARE @Pay_Amount money
DECLARE @Pay_Authoirzation nvarchar(100)
DECLARE @Pay_Gateway nvarchar(100)
DECLARE @Pay_Kind nvarchar(100)
DECLARE @Pay_Id int
DECLARE @Pay_Status nvarchar(50)
DECLARE @Financial_Status nvarchar(50)
DECLARE @Ship_Address_ID nvarchar(100)
DECLARE @Ship_First_Name nvarchar(100)
DECLARE @Ship_Last_Name nvarchar(100)
DECLARE @Ship_Company nvarchar(100)
DECLARE @Ship_Address nvarchar(100)
DECLARE @Ship_Address2 nvarchar(100)
DECLARE @Ship_Address3 nvarchar(100)
DECLARE @Ship_City nvarchar(100)
DECLARE @Ship_Province_Code nvarchar(3)
DECLARE @Ship_Province_Name nvarchar(100)
DECLARE @Ship_Post_Code nvarchar(20)
DECLARE @Ship_Country_Code nvarchar(3)
DECLARE @Ship_Country_Name nvarchar(100)
DECLARE @Ship_Latitude nvarchar(20)
DECLARE @Ship_Longitude nvarchar(20)
DECLARE @Fulfillment_Status nvarchar(50)
DECLARE @Freight_Title nvarchar(50)
DECLARE @Freight_Carrier nvarchar(50)
DECLARE @Freight_Code nvarchar(50)
DECLARE @Taxes_Included bit
DECLARE @Item_Id bigint
SET @Item_Id = '20568359'
DECLARE @Item_Price money
SET @Item_Price = '20'
DECLARE @Item_Sku nvarchar(50)
SET @Item_Sku = 'BasicSKU'
DECLARE @Item_Quantity int
SET @Item_Quantity = '10'
DECLARE @Order_Line_Number bigint
SET @Order_Line_Number = '1236548952123'
DECLARE @Total_Discount money
DECLARE @Total_Lines money
DECLARE @Total_Freight money
SET @Total_Freight = '32'
DECLARE @Total_Order money 
SET @Total_Order = '232'
DECLARE @Total_Tax money
DECLARE @Total_Tax_Rate float
DECLARE @Total_Tax_Validated money
DECLARE @WebOrderID nvarchar(30)
SET @WebOrderID = 'W1003569'
DECLARE @Bill_Phone nvarchar(20)
DECLARE @Ship_Phone nvarchar(20)
DECLARE @Line_Discount_Percent int
DECLARE @Line_Discount_Amount money

-- TODO: Set parameter values here.

EXECUTE @RC = [dbo].[FTC_SP_InsertOrder] 
   @Store_Id
  ,@Store_URI
  ,@ERP_Id
  ,@Store_Customer
  ,@ERP_Customer
  ,@Store_Status
  ,@Created_At
  ,@Updated_At
  ,@Note
  ,@Accepts_Marketing
  ,@Bill_Address_ID
  ,@Bill_First_Name
  ,@Bill_Last_Name
  ,@Bill_Company
  ,@Bill_Address
  ,@Bill_Address2
  ,@Bill_Address3
  ,@Bill_City
  ,@Bill_Province_Code
  ,@Bill_Province_Name
  ,@Bill_Post_Code
  ,@Bill_Country_Code
  ,@Bill_Country_Name
  ,@Bill_Latitude
  ,@Bill_Longitude
  ,@Cart_Token
  ,@Email
  ,@Tags
  ,@Pay_Amount
  ,@Pay_Authoirzation
  ,@Pay_Gateway
  ,@Pay_Kind
  ,@Pay_Id
  ,@Pay_Status
  ,@Financial_Status
  ,@Ship_Address_ID
  ,@Ship_First_Name
  ,@Ship_Last_Name
  ,@Ship_Company
  ,@Ship_Address
  ,@Ship_Address2
  ,@Ship_Address3
  ,@Ship_City
  ,@Ship_Province_Code
  ,@Ship_Province_Name
  ,@Ship_Post_Code
  ,@Ship_Country_Code
  ,@Ship_Country_Name
  ,@Ship_Latitude
  ,@Ship_Longitude
  ,@Fulfillment_Status
  ,@Freight_Title
  ,@Freight_Carrier
  ,@Freight_Code
  ,@Taxes_Included
  ,@Item_Id
  ,@Item_Price
  ,@Item_Sku
  ,@Item_Quantity
  ,@Order_Line_Number
  ,@Total_Discount
  ,@Total_Lines
  ,@Total_Freight
  ,@Total_Order
  ,@Total_Tax
  ,@Total_Tax_Rate
  ,@Total_Tax_Validated
  ,@WebOrderID
  ,@Bill_Phone
  ,@Ship_Phone
  ,@Line_Discount_Percent
  ,@Line_Discount_Amount
GO

USE FT_Deployment_Script_Test
GO

SELECT * FROM ORDERS
SELECT * FROM ORDERLINES